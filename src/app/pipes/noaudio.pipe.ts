import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'noaudio'
})
export class NoaudioPipe implements PipeTransform {

  transform(external_urls: any[]): string {

  if (!external_urls) {

    return 'assets/audio/katamory.mp3';

  }

  if(external_urls.length > 0){

    return external_urls[0].url;

  }else{

    return 'assets/audio/katamory.mp3';

  }

}

}
