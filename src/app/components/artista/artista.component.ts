import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { SpotifyService } from 'src/app/services/spotify.service';



@Component({
  selector: 'app-artista',
  templateUrl: './artista.component.html',
  styles: []
})
export class ArtistaComponent{

  artista: any = {};
  topTracks: any[] = [];

  LoadingArtist: boolean;
  constructor( private router: ActivatedRoute,
               private spotify: SpotifyService ) {


    this.LoadingArtist = true;

    this.router.params.subscribe( params => {
     this.getArtista( params['id'] );
     this.getTopTracks( params['id'] );
    });

   }

   getArtista( id: string ){
    this.LoadingArtist = true;
     this.spotify.getartista( id )
      .subscribe( artista => {
      this.artista = artista;

      this.LoadingArtist = false;
      });
   }

  getTopTracks( id: string ){

  this.spotify.getTopTracks( id )
  .subscribe( TopTracks => {
    this.topTracks = TopTracks;
  });

  }

}
